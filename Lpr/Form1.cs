﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Emgu.CV;
using Emgu.Util;
using Emgu.CV.Structure;
using Emgu.CV.OCR;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using System.Diagnostics;

namespace Lpr
{
    public partial class Form1 : Form
    {
        private string imageFolder = "";
        private string[] folderImages;
        private BackgroundWorker worker = new BackgroundWorker();
        private Image<Bgr, byte> currentLicensePlateImage;
        private LicensePlateDetector licensePlateDetector;
        private string lastLicensePlateFolder = "";

        public Form1()
        {
            InitializeComponent();
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(worker_DoWork);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(worker_RunWorkerCompleted);
            lastLicensePlateFolder = Properties.Settings.Default.LastLicensePlateFolder;

            licensePlateDetector = new LicensePlateDetector("");

        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnSelectFolder_Click(object sender, EventArgs e)
        {
            // allow the user to select a folder with license plate images
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.ShowNewFolderButton = true;
            // load the last selected directory for a better UX
            if(Directory.Exists(this.lastLicensePlateFolder))
            {
                fbd.SelectedPath = this.lastLicensePlateFolder;
            }

            if(fbd.ShowDialog() == DialogResult.OK)
            {
                // store the selected folder in the settings
                Properties.Settings.Default.LastLicensePlateFolder = fbd.SelectedPath;
                Properties.Settings.Default.Save();
                this.lastLicensePlateFolder = fbd.SelectedPath;

                // find all images in the folder
                this.imageFolder = fbd.SelectedPath;
                this.findImages();
            }
        }

        private void findImages()
        {
            // Find all images in a folder, first make sure the directory exists
            if (this.imageFolder != "" && Directory.Exists(this.imageFolder) == true)
            {
                this.folderImages = Directory.GetFiles(this.imageFolder, "*.jpg", SearchOption.AllDirectories);
                foreach (var img in this.folderImages)
                {
                    string fileName = Path.GetFileName(img);
                    listBoxLoadedImages.Items.Add(fileName);
                }
            } else {
                // Na-Ah, error
                MessageBox.Show("Please select a folder to process!");
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            this.startProcessing();
            btnCancel.Enabled = true;
            btnStart.Enabled = false;
        }

        private void startProcessing()
        {
            if(worker.IsBusy != true)
            {
                worker.RunWorkerAsync();
            }
        }

        private void worker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = sender as BackgroundWorker;
            int maxValue = this.folderImages.Length;
            int currentValue = 0;

            foreach (var img in this.folderImages)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    break;
                }
                else
                {
                    // run the license plate detection
                    this.processImage(img);
                    ++currentValue;
                    // calculate the progress to show in the progressbar
                    double workerProgress = (currentValue * 100 / maxValue);
                    worker.ReportProgress(Convert.ToInt32(workerProgress));
                }
            }
        }

        private void processImage(string img)
        {
            if (this.currentLicensePlateImage != null)
            {
                // dispose old image to save memory
                this.currentLicensePlateImage.Dispose();
            }
            Mat m = new Mat(img, LoadImageType.AnyColor);
            m = CvInvoke.Imread(img, LoadImageType.Color);
            UMat um = m.ToUMat(AccessType.ReadWrite);
            currentImage.Image = um;

            Image<Bgr, byte>blurred = um.ToImage<Bgr, byte>().SmoothGaussian(3);
            currentImage.Image = blurred;
            startDetection(um);
            //startDetection(blurred.ToUMat());
        }

        // Run the license plate detection
        private void startDetection(IInputOutputArray image)
        {
            Stopwatch watch = Stopwatch.StartNew();

            List<IInputOutputArray> licensePlateImagesList = new List<IInputOutputArray>();
            List<IInputOutputArray> filteredLicensePlateImagesList = new List<IInputOutputArray>();
            List<RotatedRect> licenseBoxList = new List<RotatedRect>();
            List<string> words = licensePlateDetector.DetectLicensePlate(image, licensePlateImagesList, filteredLicensePlateImagesList, licenseBoxList);

            watch.Stop();

            Point startPoint = new Point(10, 10);
            for (int i = 0; i < words.Count; i++)
            {
                Mat dest = new Mat();
                CvInvoke.VConcat(licensePlateImagesList[i], filteredLicensePlateImagesList[i], dest);
                AddLabelAndImage(
               ref startPoint,
               String.Format("{0}", words[i]),
               dest);
                PointF[] verticesF = licenseBoxList[i].GetVertices();
                Point[] vertices = Array.ConvertAll(verticesF, Point.Round);
                using (VectorOfPoint pts = new VectorOfPoint(vertices))
                    CvInvoke.Polylines(image, pts, true, new Bgr(Color.Red).MCvScalar, 2);
            }
        }
        private void AddLabelAndImage(ref Point startPoint, String labelText, IImage image)
        { 
            currentImageProcess.Image = image;
            this.Invoke((MethodInvoker)(() => listBoxMatchedPlates.Items.Add(labelText)));
        }

        private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if((e.Cancelled == true))
            {
                // canceled
                
            }
            else if(!(e.Error == null))
            {
                // error
                MessageBox.Show(e.Error.ToString());
            }
            else
            {
                // done
                this.Text = "LPR";
            }
            this.progressBar1.Value = 0;
            btnCancel.Enabled = false;
            btnStart.Enabled = true;
        }

        private void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            // provide graphical feedback
            this.progressBar1.Value = (e.ProgressPercentage);
            this.Text = String.Format("LPR :: Progress: {0}%", e.ProgressPercentage);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if(worker.IsBusy == true)
            {
                worker.CancelAsync();
            }
        }
    }
}
